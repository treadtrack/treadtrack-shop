<table style="border:0;border-collapse:collapse;width:760px" cellpadding="0" cellspacing="0">
    <tbody><tr><td colspan="3" style="vertical-align:top">
        <a href="{{ asset('') }}" target="_blank" style="text-decoration:none"><img src="{{ asset('img/logo.png') }}" alt="ukrtvsat.com" style="border-width:0;display:block"></a>
      </td></tr>
    <tr><td style="width:35px;vertical-align:top"></td>
      <td style="width:694px;vertical-align:top">
        <table style="border:0;border-collapse:collapse;width:100%" cellpadding="0" cellspacing="0">
          <tbody><tr><td style="padding-left:12px;padding-top:30px;padding-bottom:20px;vertical-align:top">
              <table style="border:0;border-collapse:collapse;width:100%" cellpadding="0" cellspacing="0">
                <tbody><tr><td style="font-size:15px;padding-bottom:18px">Здравствуйте, {{ $customer }}!</td></tr>
                <tr><td style="font-size:15px;font-weight:bold;line-height:24px;padding-bottom:16px">
                                          Ваша заявка принята. 
                                                                                                                                                                                                Для подтверждения заказа № 29401286 наш
                  <br>менеджер свяжется с Вами в ближайшее время.
                                    </td></tr>
                <tr><td style="font-size:15px;padding-bottom:16px">Статус заявки Вы можете отследить <a href="https://my.rozetka.com.ua/profile/account/?utm_source=dm&amp;utm_medium=email&amp;utm_campaign=payorder&amp;utm_content=MyProfile" target="_blank" style="color:#104889">в своем личном кабинете.</a></td></tr>
                <tr><td style="color:#333333;font-size:15px;line-height:24px">Будем рады ответить на Ваши вопросы по телефонам:<br>{{ Config::get('setting.phone') }}</td></tr>
              </tbody></table>
            </td></tr>
          <tr><td style="padding-bottom:20px;vertical-align:top">
                                              <table style="border:0;border-collapse:collapse;width:100%" cellpadding="0" cellspacing="0">
                  <tbody><tr><td style="vertical-align:top"><img src="http://i.rozetka.ua/design/letters/multicheckout/border-top.jpg" style="border-width:0;display:block" width="694" height="14"></td></tr>
                  <tr><td style="background-color:#ffffff;border-left-width:1px;border-left-style:solid;border-left-color:#eaeaea;border-right-width:1px;border-right-style:solid;border-right-color:#eaeaea;vertical-align:top">
                      <table style="border:0;border-collapse:collapse;margin-bottom:25px;line-height:24px;width:692px" cellpadding="0" cellspacing="0">
                        <tbody><tr>
                          <td colspan="2" style="vertical-align:baseline"><h2 style="font-size:24px;margin:0;padding-left:10px">Заказ № {{ $order }}</h2></td>
                          <td colspan="2" style="vertical-align:baseline;color:#666666;font-size:12px;padding-right:20px;text-align:right">17 мая 2015 16:46</td>
                        </tr>
                        <tr>
                          <td style="font-size:15px;padding-top:7px;padding-bottom:10px;padding-left:10px;width:280px;vertical-align:top">Название и цена товара</td>
                          <td style="font-size:15px;padding-top:7px;padding-bottom:10px;width:140px;vertical-align:top">Код товара</td>
                          <td style="font-size:15px;padding-top:7px;padding-bottom:10px;width:100px;vertical-align:top">Кол-во</td>
                          <td style="font-size:15px;padding-top:7px;padding-bottom:10px;text-align:right;padding-right:38px;vertical-align:top">Сумма</td>
                        </tr>
                        <tr>
                          <td colspan="4" style="border-top:1px solid #e3e3e3;vertical-align:top">
                            @foreach($cart as $product)
                            <table style="border:0;border-collapse:collapse;margin-top:10px;margin-bottom:10px;width:100%" cellpadding="0" cellspacing="0">
                              <tbody>
                                <tr>
                                  <td style="padding-left:20px;padding-right:20px;padding-top:5px;padding-bottom:5px;width:100px;vertical-align:top">
                                    <a href="{{ asset('product/') }}{{ Product::where('id', '=', $product->id)->first()->link }}" target="_blank" style="text-decoration:none"><img src="{{ asset('uploads/images/product/') }}{{ $product->id }}_1_big.png" style="border-width:0;display:block;margin:auto"></a>
                                  </td>
                                  <td style="padding-top:5px;padding-bottom:5px;vertical-align:top">
                                    <table style="border:0;border-collapse:collapse;width:100%" cellpadding="0" cellspacing="0">
                                      <tbody>
                                        <tr>
                                          <td colspan="4" style="vertical-align:top"><a href="http://rozetka.com.ua/sony_37057/p2113212/?utm_source=dm&amp;utm_medium=email&amp;utm_campaign=payorder&amp;utm_content=title" target="_blank" style="color:#104889;font-size:15px">Батарейки Sony СR2032 Lithium 5 шт (37057)</a></td>
                                        </tr>
                                        <tr>
                                          <td style="font-size:15px;width:148px;vertical-align:top">{{ $product->price }} грн.</td>
                                          <td style="font-size:15px;width:139px;vertical-align:top">{{ $product->id }}</td>
                                          <td style="font-size:15px;vertical-align:top">{{ $product->qty }} шт.</td>
                                          <td style="font-size:15px;font-weight:bold;padding-right:20px;vertical-align:top;text-align:right">{{ $product->price }} грн. </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            @endforeach
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2" style="border-top:1px solid #e3e3e3;font-size:15px;padding-left:10px;padding-top:5px;padding-bottom:5px;vertical-align:top">Доставка:</td>
                          <td colspan="2" style="border-top:1px solid #e3e3e3;padding-right:20px;padding-top:5px;padding-bottom:5px;text-align:right;vertical-align:top">
                                                          <div style="font-size:15px">бесплатно</div>
                                                      </td>
                        </tr>
                        <tr>
                          <td colspan="2" style="border-top:1px solid #e3e3e3;font-size:15px;padding-left:10px;padding-top:15px;vertical-align:baseline">К оплате:</td>
                          <td colspan="2" style="border-top:1px solid #e3e3e3;font-size:23px;font-weight:bold;padding-right:20px;padding-top:15px;text-align:right;vertical-align:baseline">31 грн.</td>
                        </tr>
              <tr><td colspan="4" style="color:#666666;font-size:12px;padding-left:10px;padding-right:20px;vertical-align:top">
                                                      </td></tr>
                      </tbody></table>
                      <table style="border:0;border-collapse:collapse;margin-top:10px;margin-bottom:10px;line-height:24px;width:692px" cellpadding="0" cellspacing="0">
                        <tbody><tr>
                          <td style="font-size:15px;padding-left:10px;padding-right:10px;vertical-align:top;width:190px">Метод доставки:</td>
                          <td style="font-size:15px;vertical-align:top">Самовывоз</td>
                        </tr>
                        <tr>
                          <td style="font-size:15px;padding-right:10px;padding-left:10px;vertical-align:top;width:190px">Служба доставки:</td>
                          <td style="font-size:15px;vertical-align:top">
                                                          из нашего магазина                                                                                                                </td>
                        </tr>
                        <tr>
                          <td style="font-size:15px;padding-right:10px;padding-left:10px;vertical-align:top;width:190px">Способ оплаты:</td>
                          <td style="font-size:15px;vertical-align:top;padding-bottom:12px;padding-top: 10px">
                                                          Наличная
                                                      </td>
                        </tr>
                      </tbody></table>
                      <table style="border:0;border-collapse:collapse;margin-top:10px;margin-bottom:10px;line-height:24px;width:692px" cellpadding="0" cellspacing="0">
                        <tbody><tr>
                          <td style="font-size:15px;padding-right:10px;padding-left:10px;vertical-align:top;width:190px">Имя и фамилия:</td>
                          <td style="font-size:15px;vertical-align:top">{{ $customer }}</td>
                        </tr>
                        <tr>
                          <td style="font-size:15px;padding-right:10px;padding-left:10px;vertical-align:top;width:190px">Телефон:</td>
                          <td style="font-size:15px;vertical-align:top">{{ $phone }}</td>
                        </tr>
                        <tr>
                          <td style="font-size:15px;padding-right:10px;padding-left:10px;vertical-align:top;width:190px">Адрес пункта самовывоза:</td>
                          <td style="font-size:15px;vertical-align:top">{{ Config::get('setting.address') }}</td>
                        </tr>
                        <tr>
                                                  </tr>
                      </tbody></table>
                    </td></tr>
                  <tr><td style="vertical-align:top"><img src="http://i.rozetka.ua/design/letters/multicheckout/border-bottom.jpg" style="border-width:0;display:block" width="694" height="13"></td></tr>
                </tbody></table>
                          </td></tr>
          <tr><td style="padding-left:12px;vertical-align:top">
              <table style="border:0;border-collapse:collapse;width:100%" cellpadding="0" cellspacing="0">
                <tbody><tr><td style="vertical-align:top;color:#333333;font-size:15px;padding-top:10px;padding-bottom:21px">Присоединяйтесь к нам:</td></tr>
                <tr><td style="vertical-align:top">
                    <table style="border:0;border-collapse:collapse" cellpadding="0" cellspacing="0">
                      <tbody><tr>
                        <td style="padding-right:35px;vertical-align:top">
                          <a target="_blank" style="color:#104889;display:block;width:94px;text-decoration:none"><img src="http://i.rozetka.ua/design/letters/multicheckout/vk.jpg" style="border-width:0;display:block" width="94" height="22"></a></td>
                        <td style="padding-right:35px;vertical-align:top">
                          <a target="_blank" style="color:#104889;display:block;width:91px;text-decoration:none"><img src="http://i.rozetka.ua/design/letters/multicheckout/fb.jpg" style="border-width:0;display:block" width="91" height="22"></a></td>
                        <td style="padding-right:35px;vertical-align:top">
                          <a target="_blank" style="color:#104889;display:block;width:73px;text-decoration:none"><img src="http://i.rozetka.ua/design/letters/multicheckout/tw.jpg" style="border-width:0;display:block" width="73" height="22"></a></td>
                      </tr>
                    </tbody></table>
                  </td></tr>
                <tr><td style="color:#333333;font-size:15px;padding-top:20px;vertical-align:top">С уважением, команда {{ asset('') }}</td></tr>
                <tr><td style="color:#999999;font-size:13px;padding-top:6px;padding-bottom:10px;vertical-align:top">Данное письмо создано автоматически, пожалуйста, не отвечайте на него.</td></tr>
              </tbody></table>
            </td></tr>
        </tbody></table>
      </td>
      <td style="width:31px;vertical-align:top"></td></tr>
  </tbody>
</table>