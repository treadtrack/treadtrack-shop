<?php
return [
    'portfolio.image.size' => ['w' => 200, 'h' => 300],
    'phone' => '+38 (050) 233 61 20',
    'email' => 'treadtrack@gmail.com',
    'logo' => ['path' => 'images/logo.png' , 'w' => 233, 'h' => 54],
    'favicon' => ['name' => 'norm','path' => 'images/favicon.png' , 'w' => 16, 'h' => 16],
    'company' => 'TreadTrack Shop',
    'title' => 'Интернет магазин спутникового оборудования',
    'description' => 'Интернет магазин спутникового оборудования, большой выбор спутниковых антенн',
    'author' => 'Костенко Алексей',
    'keywords' => 'спутник, антенна, интернет, магазин, тв, телевидение',
    'product.image.size' => [
            ['name' => 'max', 'w' => 433, 'h' => 325],
            ['name' => 'big', 'w' => 246, 'h' => 186],
            ['name' => 'norm', 'w' => 194, 'h' => 143],
            ['name' => 'thumb', 'w' => 73, 'h' => 73],
        ],
    'logo.size' => [
            ['name' => 'norm', 'w' => 233, 'h' => 54],
        ],
    'icon.size' => [
            ['name' => 'norm', 'w' => 16, 'h' => 16],
        ],
    'address' => 'Киев, Украина',
    'exchangeRates' => 20,
    'mode' => '<p><small>С понедельника по пятницу: с 8:00 до 21:00<br />
суббота: с 9:00 до 18:00<br />
воскресенье: выходной</small></p>
',
    'zoom' => '10',
    'latitude' => '50.430015',
    'longitude' => '30.536490',
];